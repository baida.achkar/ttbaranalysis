#! /bin/bash
# Print the environment
echo $(hostname)
cat /proc/cpuinfo
echo "Job Start Time: $(date)"
## small script to run TTbarAnalysis

##OPTION: option = 0 (run all data and MC one after the other), parallel = 0 (parallel mode disabled)
option=11
parallel=0
mkdir Output_TTbarAnalysis
mkdir Histograms

## execute and run ROOT
echo "starting ROOT"
##
COMMAND="root -b -x -q main_TTbarAnalysis.C+($parallel,$option)"
apptainer exec root_latest.sif ${COMMAND}
##
echo "end of ROOT execution"
## generic cleaning of temporary files after linking and compilation
rm -rf *_ACLiC_*
rm -rf *.d
rm -rf *.pcm
rm -rf *.so
echo "Job Finished at $(date)"
